hex:
	avr-gcc -g -Os -mmcu=atmega328p -c blinky.c blinky.h
	avr-gcc -g -mmcu=atmega328p -o blinky.elf blinky.o
	avr-objcopy -j .text -j .data -O ihex blinky.elf blinky.hex
