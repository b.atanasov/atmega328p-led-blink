#ifndef _BLINKY_H_
#define _BLINKY_H_

/***
SPECIAL FUNCTION REGISTER OFFSET */
#define _SFR_OFFSET					0x20

/***
SPECIAL FUNCTION REGISTER MACROS */
#define _SFR_8BIT(addr)				((addr) + _SFR_OFFSET)

/***
MEMORY ADDRESSES */
#define _PORTB 						_SFR_8BIT(0x05)
#define _DDRB 						_SFR_8BIT(0x04)

/***
BIT PIN OFFSETS */
#define _LED_BIT 					5
#endif /* _BLINKY_H_ */